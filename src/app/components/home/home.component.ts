import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent  
{
   newReleases: any[]=[];
  constructor(private spotifyService:SpotifyService)
  {
    this.spotifyService.getNewReleases()
    .subscribe((data:any)=>
      {
        console.log(data.albums.items);
        this.newReleases=data.albums.items;

      });
   
  }
  

}
