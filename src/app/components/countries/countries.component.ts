import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  listCountries:any[]=[];
  constructor(private http:HttpClient) 
  {
    console.log('done countries constructor ');
    this.http.get('https://restcountries.eu/rest/v2/lang/es').subscribe
      (
        (response:any)=>
        {
          this.listCountries=response;
          console.log(response);
        }
      )
  }

  ngOnInit() {
  }

}
